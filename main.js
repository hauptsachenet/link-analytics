(function($, ga) {

    "use strict";

    var $document = $(document);



    var REGEX_DOWNLOAD = /\.(zip|exe|dmg|pdf|doc.*|xls.*|ppt.*|mp3|txt|rar|wma|mov|avi|wmv|flv|wav)(\?.*)?$/i;
    var REGEX_EMAIL = /^mailto\:|linkTo_UnCryptMailto/i;
    var REGEX_PHONE = /^tel:/i;
    var REGEX_JAVASCRIPT = /^Javascript\:/i;
    var REGEX_ACTION = /^\w+\:/i;

    /**
     * contains an element that should be ignored.
     * This will be set after a click on a link which should result into leaving the page.
     * Leaving the page may be delayed to give analytics a chance to send an event (only webkit)
     */
    var clickDelayedElement = null;

    $document.on('click.ga', 'a[href], [data-ga-event-category]', function (evt) {

        if (clickDelayedElement === this) {
            return;
        }

        var $link = $(this);
        var href = $link.prop('href');

        try {
            var category = (function () {
                var dataCategory = $link.data('ga-event-category');
                if (dataCategory) {
                    return dataCategory;
                }

                return 'link';
            })();

            var action = (function () {
                var dataAction = $link.data('ga-event-action');
                if (dataAction) {
                    return dataAction
                }

                if (REGEX_DOWNLOAD.test(href)) {
                    return 'download';
                }

                if (REGEX_EMAIL.test(href)) {
                    return 'send mail';
                }

                if (REGEX_PHONE.test(href)) {
                    return 'call'
                }

                if ($link.prop('host') !== location.host) {
                    return 'external'
                }

                if ($link.prop('host') === location.host) {
                    throw new Error("link is internal");
                }

                return 'click';
            })();

            var label = (function () {
                var dataEvent = $link.data('ga-event-label');
                if (dataEvent) {
                    return dataEvent;
                }

                if (REGEX_EMAIL.test(href)) {
                    var address = href.replace(/^(javascript|mailto|tel)\:\s*/, '');

                    if (REGEX_JAVASCRIPT.test(href)) {
                        address = eval(address.split('linkTo_UnCryptMailto').join('decryptString').split(')').join(',-1)'));
                    }

                    return address;
                }

                // if external use always the link
                if ($link.prop('host') !== location.host) {
                    return href;
                }

                // prefer the link title
                var title = $.trim($link.prop('title') || "");
                if (title !== "") {
                    return title;
                }

                // as last method use the link content
                var text = $.trim($link.text() || "");
                if (text !== "") {
                    return text;
                }

                // if everything fails use href
                return href;
            })();

            if(typeof dataLayer === 'object') {

                // Google Tag Manager

                dataLayer.push({
                    'event' : 'GAEvent',
                    'eventCategory' : category,
                    'eventAction' : action,
                    'eventLabel' : label,
                    'eventValue' : $link.data('ga-event-value')
                });

                var stayOnPage = evt.isDefaultPrevented()
                    || REGEX_ACTION.test(href);

                var delayNeeded = 'WebkitAppearance' in document.documentElement.style;

                if (!stayOnPage && delayNeeded) {
                    evt.preventDefault();
                    clickDelayedElement = this;

                    setTimeout(function () {
                        clickDelayedElement.click();
                    }, 200);
                }

            } else if (!$.isFunction(ga)) {
                console.warn("there was no ga function found");
                return;
            } else {

                ga('send', 'event', category, action, label, $link.data('ga-event-value'));

                var stayOnPage = evt.isDefaultPrevented()
                    || REGEX_ACTION.test(href);

                var delayNeeded = 'WebkitAppearance' in document.documentElement.style;

                if (!stayOnPage && delayNeeded) {
                    evt.preventDefault();
                    clickDelayedElement = this;

                    setTimeout(function () {
                        clickDelayedElement.click();
                    }, 200);
                }
            }

        } catch (e) {
            console.warn('link error', e.message, this, e);
        }
    });

})(jQuery, window.ga);